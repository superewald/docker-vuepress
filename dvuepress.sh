#!/bin/bash
MODE="-d"
CMD="$@"

if [ $# -eq 0 ]; then
    CMD="dev"
    MODE="-it"
elif [ "$1" == "dev" ]; then
    MODE="-it"
fi

docker run $MODE --rm \
    -v $(pwd):/vuepress/src \
    -v $(pwd)/dist:/vuepress/dist:rw \
    -p 8080:8080 \
    superewald/vuepress $CMD
    
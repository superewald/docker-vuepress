#!/bin/ash
DEST=${VUEPRESS_OUTPUT:=/vuepress/dist}
VUE_CMD=${VUE_CMD:="$@"}

if [ ! -f package.json ]; then
    VUE_CMD="$VUE_CMD src/"
    cd ../temp/
    yarn add -D ${PLUGINS}
else
    yarn install
fi

if [ "$1" == "build" ]; then
    VUE_CMD="$VUE_CMD -d $DIST"
    mkdir -p $DEST
fi

rm -rf .vuepress/dist
yarn vuepress $VUE_CMD
FROM node:alpine

ARG UID=1000
ARG GID=1000

USER root
RUN apk add --no-cache shadow sudo && \
    if [ -z "`getent group $GID`" ]; then \
      addgroup -S -g $GID vuepress; \
    else \
      groupmod -n vuepress `getent group $GID | cut -d: -f1`; \
    fi && \
    if [ -z "`getent passwd $UID`" ]; then \
      adduser -S -u $UID -G vuepress -s /bin/sh vuepress; \
    else \
      usermod -l vuepress -g $GID -d /home/vuepress -m `getent passwd $UID | cut -d: -f1`; \
    fi && \
    echo "vuepress ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/vuepress && \
    chmod 0440 /etc/sudoers.d/vuepress

WORKDIR /home/vuepress
COPY entrypoint.sh /home/vuepress/entrypoint.sh
RUN mkdir /vuepress
RUN chown vuepress:vuepress /home/vuepress /vuepress
USER vuepress

RUN mkdir -p /vuepress/src /vuepress/temp/.vuepress vuepress/dist && \
    ln -s /vuepress/src /vuepress/temp/src
COPY config/vuepress.js /vuepress/temp/.vuepress/config.js
COPY config/package.json /vuepress/temp/package.json
RUN sudo chown -R vuepress:vuepress /vuepress/
RUN cd /vuepress/temp/ && yarn install

VOLUME /vuepress/src
VOLUME /vuepress/dist

EXPOSE 8080

WORKDIR /vuepress/src
ENTRYPOINT [ "sh", "/home/vuepress/entrypoint.sh"]